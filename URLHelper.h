//
//  URLHelper.h
//  pfa
//
//  Created by apple on 12-6-29.
//  Copyright (c) 2012年 BOOHEE. All rights reserved.
//

#import <Foundation/Foundation.h>


@interface URLHelper : NSObject

+ (NSString *)getURL:(NSString *)baseUrl 
	 queryParameters:(NSMutableDictionary*)params;

+ (NSString *)getURL:(NSString *)baseUrl
     queryParameters:(NSMutableDictionary*)params
              isSort:(BOOL)isSort;

+ (NSString *)_queryStringWithBase:(NSString *)base
                        parameters:(NSDictionary *)params;
+ (NSString *)_queryStringWithBase:(NSString *)base
                        parameters:(NSDictionary *)params
                          prefixed:(BOOL)prefixed;
+ (NSString *)_queryStringWithBase:(NSString *)base
                        parameters:(NSDictionary *)params
                          prefixed:(BOOL)prefixed
                            isSort:(BOOL)isSort;
@end

@interface NSURL (CatagoryOfParser)

-(NSDictionary *)dictionaryOfQuery;

@end