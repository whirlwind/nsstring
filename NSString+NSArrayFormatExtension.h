//
//  NSString+NSArrayFormatExtension.h
//  food
//
//  Created by Whirlwind James on 12-3-3.
//  Copyright (c) 2012年 BOOHEE. All rights reserved.
//



@interface NSString (NSArrayFormatExtension)

+ (id)stringWithFormat:(NSString *)format array:(NSArray*) arguments;

@end
