//
//  NSStringValid.m
//  food
//
//  Created by 詹 迟晶 on 12-2-27.
//  Copyright (c) 2012年 BOOHEE. All rights reserved.
//

#import "NSStringValid.h"

@implementation NSStringValid
@synthesize allowEmpty = _allowEmpty;
@synthesize predicateArray = _predicateArray;
@synthesize predicateSelectors = _predicateSelectors;
#pragma mark - dealloc
- (void)dealloc{
    [_predicateArray release], _predicateArray = nil;
    [_predicateSelectors release], _predicateSelectors = nil;
    [super dealloc];
}

- (id)init{
    if ((self = [super init])) {
        self.allowEmpty = YES;
        _predicateArray = [[NSMutableArray alloc] initWithCapacity:0];
        _predicateSelectors = [[NSMutableArray alloc] initWithCapacity:0];
    }
    return self;
}

- (void)addPredicateString:(NSString *)string{
    [self.predicateArray addObject:string];
}

- (void)removePredicateString:(NSString *)string{
    NSString *s = nil;
    for (NSString *str in self.predicateArray) {
        if ([str isEqualToString:string]) {
            s = str;
            break;
        }
    }
    if (s != nil) {
        [self.predicateArray removeObject:s];
    }
}

- (void)clearPredicateString{
    [self.predicateArray removeAllObjects];
}

- (BOOL)evaluateWithString:(NSString *)string{
    if (string == nil || [string isEqualToString:@""]) {
        return self.allowEmpty;
    }
    for (NSString *pre in self.predicateArray) {
        NSPredicate *test = [NSPredicate predicateWithFormat:@"SELF MATCHES %@", pre];
        if (![test evaluateWithObject:string]) return NO;
    }
    NSMutableDictionary *dic = [[NSMutableDictionary alloc] initWithObjectsAndKeys:[NSNumber numberWithBool:YES], @"state", nil];
    for (NSInvocation *inv in self.predicateSelectors) {
        [inv setArgument:&string atIndex:2];
        [inv setArgument:&dic atIndex:3];
        [inv invoke];
        if (![(NSNumber *)[dic valueForKey:@"state"] boolValue]) {
            [dic release];
            return NO;
        }
    }
    [dic release];
    return YES;
}

- (void)addPredicateWithTarget:(id)target selector:(SEL)selector{
    NSMethodSignature *sig = [[target class] instanceMethodSignatureForSelector:selector];
    NSInvocation *inv = [NSInvocation invocationWithMethodSignature:sig];
    [inv setTarget:target];
    [inv setSelector:selector];
//    [inv setArgument:&args atIndex:2]; // Address of your object
    [self.predicateSelectors addObject:inv];
}

@end
