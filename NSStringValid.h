//
//  NSStringValid.h
//  food
//
//  Created by 詹 迟晶 on 12-2-27.
//  Copyright (c) 2012年 BOOHEE. All rights reserved.
//

#ifndef kNSStringValidIntegerPredicate
#   define kNSStringValidIntegerPredicate @"\\d+"
#endif
#ifndef kNSStringValidDecimalPredicate
#   define kNSStringValidDecimalPredicate @"\\d+\\.?\\d*"
#endif
#ifndef kNSStringValidEmailPredicate
#   define kNSStringValidEmailPredicate @"\\w+([-+.]\\w+)*@\\w+([-.]\\w+)*\\.\\w+([-.]\\w+)*"
#endif
#ifndef kNSStringValidDatePredicate
#   define kNSStringValidDatePredicate @"\\d{2,4}\\-(0?[1-9]|1[012])\\-(0?[1-9]|[12][0-9]|3[01])"
#endif
#ifndef kNSStringValidCellphonePredicate
#   define kNSStringValidCellphonePredicate @"1\\d{10}"
#endif

@interface NSStringValid : NSObject
@property (nonatomic, assign) BOOL allowEmpty;
@property (nonatomic, retain) NSMutableArray *predicateArray;
@property (nonatomic, retain) NSMutableArray *predicateSelectors;
- (void)addPredicateString:(NSString *)string;
- (void)removePredicateString:(NSString *)string;
- (void)clearPredicateString;
- (BOOL)evaluateWithString:(NSString *)string;
/** 添加判断的方法
 *
 *  通过传递target和selector，允许用户通过自定义方法判断是否符号验证要求
 *
 *  selector格式：
 *
 *      - (void)<#methods#>:(NSString *)value state:(NSMutableDictionary *)state
 *
 *  其中的state参数是一个字典，里面只有一个key:@"state"，对于一个NSNumber类型的BOOL，当验证失败时，应该使用：[state setValue:[NSNumber numberWithBool:NO] forKey:@"state"]
 *  @param  target  验证方法所在的实例类
 *  @param  selector 验证方法
 *  @see    evaluateWithString:
 */
- (void)addPredicateWithTarget:(id)target selector:(SEL)selector;
@end
