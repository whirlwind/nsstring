//
//  URLHelper.m
//  pfa
//
//  Created by apple on 12-6-29.
//  Copyright (c) 2012年 BOOHEE. All rights reserved.
//

#import "URLHelper.h"

@implementation URLHelper


+ (NSString *)_encodeString:(NSString *)string
{
    NSString *result = (NSString *)CFURLCreateStringByAddingPercentEscapes(NULL, 
																		   (CFStringRef)string, 
																		   NULL, 
																		   (CFStringRef)@";/?:@&=$+{}<>,",
																		   kCFStringEncodingUTF8);
    return [result autorelease];
}


+ (NSString *)_queryStringWithBase:(NSString *)base parameters:(NSDictionary *)params {
    return [self _queryStringWithBase:base parameters:params prefixed:YES];
}

+ (NSString *)_queryStringWithBase:(NSString *)base parameters:(NSDictionary *)params prefixed:(BOOL)prefixed
{
        
    return [self _queryStringWithBase:base parameters:params prefixed:prefixed isSort:NO];
}

+ (NSString *)_queryStringWithBase:(NSString *)base parameters:(NSDictionary *)params prefixed:(BOOL)prefixed isSort:(BOOL)isSort
{
    // Append base if specified.
    NSMutableString *str = [NSMutableString stringWithCapacity:0];
    if (base) {
        [str appendString:base];
    }
    
    // Append each name-value pair.
    if (params) {
        int i;
        NSArray *names = nil;
        if(isSort){
            names = [[params allKeys] sortedArrayUsingSelector:@selector(compare:)];
        }else{
            names = [params allKeys];
        }
        
        for (i = 0; i < [names count]; i++) {
            if (i == 0 && prefixed && [str rangeOfString:@"?"].location == NSNotFound) {
                [str appendString:@"?"];
            } else {
                [str appendString:@"&"];
            }
            NSString *name = nil;
            NSString *encoderStr = nil;
            if([[names objectAtIndex:i] isKindOfClass:[NSArray class]]){
                for(int j=0;j<[[names objectAtIndex:i] count];j++){
                    encoderStr = [[params objectForKey:name] isKindOfClass:[NSString class]]
                    ? [params objectForKey:name] : [[params objectForKey:name] description];
                    [str appendString:[NSString stringWithFormat:@"%@=%@",
                                       name, [self _encodeString:encoderStr]]];
                }
            }else{
                name = [names objectAtIndex:i];
                encoderStr = [[params objectForKey:name] isKindOfClass:[NSString class]]
                ? [params objectForKey:name] : [[params objectForKey:name] description];
                [str appendString:[NSString stringWithFormat:@"%@=%@",
                                   name, [self _encodeString:encoderStr]]];
            }
        }
    }
    
    return str;
}


+ (NSString *)getURL:(NSString *)baseUrl 
	 queryParameters:(NSMutableDictionary*)params {
    NSString* fullPath = [[baseUrl copy] autorelease];
	if (params) {
        fullPath = [self _queryStringWithBase:fullPath parameters:params prefixed:YES];
    }
	return fullPath;
}
+ (NSString *)getURL:(NSString *)baseUrl
     queryParameters:(NSMutableDictionary*)params isSort:(BOOL)isSort{
    NSString* fullPath = [[baseUrl copy] autorelease];
	if (params) {
        fullPath = [self _queryStringWithBase:fullPath parameters:params prefixed:YES isSort:isSort];
    }
    return fullPath;
}

@end

@implementation NSURL (CatagoryOfParser)

-(NSDictionary *)dictionaryOfQuery
{
    NSString *string =  [[self.absoluteString stringByReplacingOccurrencesOfString:@"+" withString:@" "]
                         stringByReplacingPercentEscapesUsingEncoding:NSUTF8StringEncoding];
    NSScanner *scanner = [NSScanner scannerWithString:string];
    [scanner setCharactersToBeSkipped:[NSCharacterSet characterSetWithCharactersInString:@"&?"]];
    
    NSString *temp;
    NSMutableDictionary *dict = [[[NSMutableDictionary alloc] init] autorelease];
    [scanner scanUpToString:@"?" intoString:nil];       //ignore the beginning of the string and skip to the vars
    while ([scanner scanUpToString:@"&" intoString:&temp])
    {
        NSArray *parts = [temp componentsSeparatedByString:@"="];
        if([parts count] == 2)
        {
            [dict setObject:[parts objectAtIndex:1] forKey:[parts objectAtIndex:0]];
        }
    }
    
    return dict;
}

@end