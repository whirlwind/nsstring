//
//  NSString+NSArrayFormatExtension.m
//  food
//
//  Created by Whirlwind James on 12-3-3.
//  Copyright (c) 2012年 BOOHEE. All rights reserved.
//

#import "NSString+NSArrayFormatExtension.h"

@implementation NSString (NSArrayFormatExtension)

+ (id)stringWithFormat:(NSString *)format array:(NSArray*) arguments;
{
    int l = sizeof(NSString *);
    char *argList = (char *)malloc(l * [arguments count]);
    [arguments getObjects:(id *)argList];
    NSString* result = [[[NSString alloc] initWithFormat:format arguments:argList] autorelease];
    free(argList);
    return result; // (Quinn) Should be autoreleased to obey Cocoa convention
}

@end
